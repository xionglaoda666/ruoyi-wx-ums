package com.ruoyi.system.api.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 授权信息对象 ums_account_auth
 * 
 * @author ruoyi
 * @date 2021-03-03
 */
public class UmsAccountAuth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private String accountId;

    /** 授权类型 */
    @Excel(name = "授权类型")
    private String identityType;

    /** 平台唯一标识 */
    @Excel(name = "平台唯一标识")
    private String identifier;

    /** access_token类型内容 */
    @Excel(name = "access_token类型内容")
    private String credential;

    /** 授权状态(0正常，1取消) */
    @Excel(name = "授权状态(0正常，1取消)")
    private String status;

    /** 绑定时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "绑定时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date bindTime;

    /** 认证返回json数据 */
    private String authData;


    /** accessToken有效期 */
    @Excel(name = "accessToken有效期")
    private Long expireIn;

    /** 刷新token */
    @Excel(name = "刷新token")
    private String refreshToken;

    /** $column.columnComment */
    @Excel(name = "刷新token")
    private Long refreshTokenExpireIn;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getAccountId()
    {
        return accountId;
    }
    public void setIdentityType(String identityType)
    {
        this.identityType = identityType;
    }

    public String getIdentityType()
    {
        return identityType;
    }
    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public String getIdentifier()
    {
        return identifier;
    }
    public void setCredential(String credential)
    {
        this.credential = credential;
    }

    public String getCredential()
    {
        return credential;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setBindTime(Date bindTime)
    {
        this.bindTime = bindTime;
    }

    public Date getBindTime()
    {
        return bindTime;
    }
    public void setAuthData(String authData)
    {
        this.authData = authData;
    }

    public String getAuthData()
    {
        return authData;
    }
    public void setExpireIn(Long expireIn)
    {
        this.expireIn = expireIn;
    }

    public Long getExpireIn()
    {
        return expireIn;
    }
    public void setRefreshToken(String refreshToken)
    {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }
    public void setRefreshTokenExpireIn(Long refreshTokenExpireIn)
    {
        this.refreshTokenExpireIn = refreshTokenExpireIn;
    }

    public Long getRefreshTokenExpireIn()
    {
        return refreshTokenExpireIn;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("accountId", getAccountId())
                .append("identityType", getIdentityType())
                .append("identifier", getIdentifier())
                .append("credential", getCredential())
                .append("status", getStatus())
                .append("bindTime", getBindTime())
                .append("authData", getAuthData())
                .append("expireIn", getExpireIn())
                .append("refreshToken", getRefreshToken())
                .append("refreshTokenExpireIn", getRefreshTokenExpireIn())
                .toString();
    }
}

package com.ruoyi.system.api.model;

import com.ruoyi.system.api.domain.UmsAccountAuth;
import com.ruoyi.system.api.domain.UmsAccountInfo;

public class ThirdAuthUser {
    private UmsAccountInfo umsAccountInfo;
    private UmsAccountAuth umsAccountAuth;

    public UmsAccountInfo getUmsAccountInfo() {
        return umsAccountInfo;
    }

    public void setUmsAccountInfo(UmsAccountInfo umsAccountInfo) {
        this.umsAccountInfo = umsAccountInfo;
    }

    public UmsAccountAuth getUmsAccountAuth() {
        return umsAccountAuth;
    }

    public void setUmsAccountAuth(UmsAccountAuth umsAccountAuth) {
        this.umsAccountAuth = umsAccountAuth;
    }
}

package com.ruoyi.system.api;

import cn.hutool.json.JSONObject;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.UmsAccountAuth;
import com.ruoyi.system.api.factory.RemoteUmsFallbackFactory;
import com.ruoyi.system.api.model.ThirdAuthUser;
import me.zhyd.oauth.model.AuthUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(contextId = "feignUmsService",value = "ruoyi-ums",fallbackFactory = RemoteUmsFallbackFactory.class)
public interface FeignUmsService {
    /**
     * 通过用户名查询用户信息
     *
     * @param authUser 结果json数据
     * @return 结果
     */
    @PostMapping(value = "/auth/info")
    R<ThirdAuthUser> getUserInfo(@RequestBody AuthUser authUser);
}

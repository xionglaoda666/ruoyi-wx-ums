'use strict';
const path = require('path');
const defaultSettings = require('./src/settings.js');
function resolve(dir){
    return path.join(__dirname,dir);
}

const name= defaultSettings.title ||'河南省科技馆LOGO征集入围作品公众投票';
const port = process.env.NODE_ENV === 'production' ? 80 : 80;
module.exports = {
    publicPath:process.env.NODE_ENV === 'production' ? "/" : '/',
    outputDir: 'vote',
    assetsDir: "static",
    productionSourceMap: false,
    lintOnSave: process.env.NODE_ENV ==='development',
    devServer: {
        /* 自动打开浏览器 */
        open: false,
        /* 设置为0.0.0.0则所有的地址均能访问 */
        host: '0.0.0.0',
        port: port,
        https: false,
        disableHostCheck: true,//关闭host检查
        hotOnly: false,
        // // /* 使用代理 */
      proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      [process.env.VUE_APP_BASE_API]: {
          // target: `http://192.168.1.85:8081`,
        target: process.env.NODE_ENV === 'production'?`http://vote.ueweixin.com`:`http://192.168.1.85:8081`,
        // target: `http://localhost:8080`,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    }
    },
    css: {
        loaderOptions: {
            less: {
                modifyVars: {
                    hack: `true; @import "~@/assets/main.less";`,
                },
            },
        },
    },
    configureWebpack:{
        name:name,
        resolve:{
            alias:{
                '@':resolve('src')
            }
        }
    }
}

const wx = require("wxsdk-api");
import {getConfig} from '@/api/login';
/*
* 判断是否IOS环境
* */
export function isIOS () {
    if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
        return true;
    }
    return false;
}

export function wxShare(title,desc,link,imgUrl) {

    getConfig({url:window.location.href}).then(res=>{
        let param = {
            debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: res.data.appId, // 必填，公众号的唯一标识
            timestamp: res.data.timestamp, // 必填，生成签名的时间戳
            nonceStr:res.data.nonceStr, // 必填，生成签名的随机串
            signature:res.data.signature,// 必填，签名
            jsApiList: ["updateAppMessageShareData","updateTimelineShareData"] // 必填，需要使用的JS接口列表
        }
        wx.config(param);
        console.log(link.lastIndexOf("voteId"))
        if(link.lastIndexOf("voteId")===-1){
            link = link+"?voteId="+localStorage.getItem("voteId")
        }
        updateAppMessageShareData(title,desc,link,imgUrl);
        updateTimelineShareData(title,link,imgUrl);
    })
}
export function updateAppMessageShareData(title,desc,link,imgUrl) {
    wx.ready(function () {   //需在用户可能点击分享按钮前就先调用
        wx.updateAppMessageShareData({
            title: title, // 分享标题
            desc: desc, // 分享描述
            link: link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: imgUrl, // 分享图标
            success: function () {
            }
        })
    });
}
export function updateTimelineShareData(title,link,imgUrl) {
    wx.ready(function () {      //需在用户可能点击分享按钮前就先调用
        wx.updateTimelineShareData({
            title: title, // 分享标题
            link: link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: imgUrl, // 分享图标
            success: function () {
            }
        })
    });


}

// date.js
export function getDate(dt) {
  let year = dt.getFullYear();
  let month = dt.getMonth()+1;
  let day = dt.getDate();

    return year +"-"+buWei(month)+"-"+buWei(day);
}

export function getDateTime(dt) {
  let year = dt.getFullYear();
  let month = dt.getMonth()+1;
  let day = dt.getDate();
  let hour = dt.getHours();
  let minutes = dt.getMinutes();
  return year +"-"+buWei(month)+"-"+buWei(day) + " "+ buWei(hour) +":"+buWei(minutes);
}

export function dateFormat(fmt, date) {
  let ret;
  const opt = {
    "Y+": date.getFullYear().toString(),        // 年
    "m+": (date.getMonth() + 1).toString(),     // 月
    "d+": date.getDate().toString(),            // 日
    "H+": date.getHours().toString(),           // 时
    "M+": date.getMinutes().toString(),         // 分
    "S+": date.getSeconds().toString()          // 秒
    // 有其他格式化字符需求可以继续添加，必须转化成字符串
  };
  for (let k in opt) {
    ret = new RegExp("(" + k + ")").exec(fmt);
    if (ret) {
      fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
    };
  };
  return fmt;
}

export function getIosDate(dt) {

  return  new Date(getDate(dt).replace(/-/g, '/'));
}

//定义一个补位的函数
function buWei(i) {
  i =  i < 10 ? "0"+i : i;
  return i;
}

var getDifferScale = function (value) {
  var format;
  //获取转化比（天数跟毫秒数的比例）
  if (value == 1) {
    format = parseFloat(24 * 60 * 60 * 1000);
  }
  //获取转化比（小时数跟毫秒数的比例）
  else if (value == 2) {
    format = parseFloat(60 * 60 * 1000);
  }
  //获取转化比（分钟数跟毫秒数的比例）
  else if (value == 3) {
    format = parseFloat(60 * 1000);
  }
  //获取转化比（秒数跟毫秒数的比例）
  else if (value == 4) {
    format = parseFloat(1000);
  }
  return format;
}

//获取两个日期的相差日期数(differ 相差天数：1、相差小时数：2、相差分钟数：3、相差秒数：4)
export function getDifferDate(firstDate, secondDate, differ) {
  //1)将两个日期字符串转化为日期对象
  var startDate = new Date(firstDate);
  var endDate = new Date(secondDate);
  //2)计算两个日期相差的毫秒数
  var msecNum = endDate.getTime() - startDate.getTime();
  //3)计算两个日期相差的天数
  var dayNum = Math.floor(msecNum /getDifferScale(differ));
  return dayNum;
}

export function tabTime(endDate){
  var oDate1 = new Date();
  if(oDate1.getTime() > endDate.getTime()){
    return false;//不在6年内
  } else {
    return true;//是六年内
  }
}

//天数增加
export function dateChange(num = 1) {
  let date = Date.parse(new Date())/1000;//转换为时间戳
  date += (86400) * num;//修改后的时间戳
  var newDate = new Date(parseInt(date) * 1000);//转换为时间
  return newDate;
}

//天数增加
export function dateChangeByDate(convertDate,num = 1) {
  let date = Date.parse(convertDate)/1000;//转换为时间戳
  date += (86400) * num;//修改后的时间戳
  var newDate = new Date(parseInt(date) * 1000);//转换为时间
  return newDate;
}


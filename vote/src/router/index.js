import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

  const routes = [
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
    {
      path: '/501',
      component: () => import('@/views/error/nosuchvote'),
      hidden: true
    },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true
  },
  {
    path: '/',
    name: 'home',
    component:() => import('@/views/index'),
    meta:{
      title:"河南省科技馆LOGO征集入围作品公众投票"
    }
  },
  {
    path: '/auth',
    name: 'auth',
    component:() => import('@/views/auth'),
  },
    {
      path: '/roles',
      name: 'roles',
      component:() => import('@/views/roles'),
    },
    {
      path: '/details',
      name: 'details',
      component:() => import('@/views/details/index'),
    },
    {
      path: '/rankingList',
      name: 'rankingList',
      component:() => import('@/views/rankinglist/index'),
    }
];

const router = new VueRouter({
  mode: process.env.NODE_ENV ==="production"?'history':"hash",
  base: process.env.NODE_ENV ==="production"?"/":"/",
  routes
})

export default router

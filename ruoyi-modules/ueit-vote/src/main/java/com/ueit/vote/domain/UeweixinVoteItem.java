package com.ueit.vote.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 投票活动选项对象 ueweixin_vote_item
 * 
 * @author cuixh
 * @date 2021-03-08
 */
public class UeweixinVoteItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 选项标题 */
    @Excel(name = "选项标题")
    private String voteItemTitle;

    /** 参赛编号 */
    @Excel(name = "参赛编号")
    private String voteItemNumber;

    /** 参赛图片 */
    @Excel(name = "参赛图片")
    private String voteItemImg;

    /** 票数 */
    @Excel(name = "票数")
    private Long voteItemCount;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String voteItemPhone;

    /** 详细内容 */
    @Excel(name = "详细内容")
    private String voteItemContent;

    private Integer orderByCount;

    private String voteToken;

    public Integer getOrderByCount() {
        return orderByCount;
    }

    public void setOrderByCount(Integer orderByCount) {
        this.orderByCount = orderByCount;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setVoteItemTitle(String voteItemTitle) 
    {
        this.voteItemTitle = voteItemTitle;
    }

    public String getVoteItemTitle() 
    {
        return voteItemTitle;
    }
    public void setVoteItemNumber(String voteItemNumber) 
    {
        this.voteItemNumber = voteItemNumber;
    }

    public String getVoteItemNumber() 
    {
        return voteItemNumber;
    }
    public void setVoteItemImg(String voteItemImg) 
    {
        this.voteItemImg = voteItemImg;
    }

    public String getVoteItemImg() 
    {
        return voteItemImg;
    }
    public void setVoteItemCount(Long voteItemCount) 
    {
        this.voteItemCount = voteItemCount;
    }

    public Long getVoteItemCount() 
    {
        return voteItemCount;
    }
    public void setVoteItemPhone(String voteItemPhone) 
    {
        this.voteItemPhone = voteItemPhone;
    }

    public String getVoteItemPhone() 
    {
        return voteItemPhone;
    }
    public void setVoteItemContent(String voteItemContent) 
    {
        this.voteItemContent = voteItemContent;
    }

    public String getVoteItemContent() 
    {
        return voteItemContent;
    }

    public String getVoteToken() {
        return voteToken;
    }

    public void setVoteToken(String voteToken) {
        this.voteToken = voteToken;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("voteItemTitle", getVoteItemTitle())
            .append("voteItemNumber", getVoteItemNumber())
            .append("voteItemImg", getVoteItemImg())
            .append("voteItemCount", getVoteItemCount())
            .append("voteItemPhone", getVoteItemPhone())
            .append("voteItemContent", getVoteItemContent())
                .append("voteToken",getVoteToken())
            .toString();
    }
}

package com.ueit.vote.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.redis.service.RedisService;
import com.ueit.vote.domain.UeweixinVote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ueit.vote.domain.UeweixinVoteItem;
import com.ueit.vote.service.IUeweixinVoteItemService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 投票活动选项Controller
 * 
 * @author cuixh
 * @date 2021-03-08
 */
@RestController
@RequestMapping("/voteItem")
public class UeweixinVoteItemController extends BaseController
{
    @Autowired
    private IUeweixinVoteItemService ueweixinVoteItemService;
    @Autowired
    private RedisService redisService;
    /**
     * 查询投票活动选项列表
     */
    @PreAuthorize(hasPermi = "voteItem:voteItem:list")
    @GetMapping("/list")
    public TableDataInfo list(UeweixinVoteItem ueweixinVoteItem)
    {
        startPage();
        List<UeweixinVoteItem> list = ueweixinVoteItemService.selectUeweixinVoteItemList(ueweixinVoteItem);
        return getDataTable(list);
    }

    /**
     * 导出投票活动选项列表
     */
    @PreAuthorize(hasPermi = "voteItem:voteItem:export")
    @Log(title = "投票活动选项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UeweixinVoteItem ueweixinVoteItem) throws IOException
    {
        List<UeweixinVoteItem> list = ueweixinVoteItemService.selectUeweixinVoteItemList(ueweixinVoteItem);
        ExcelUtil<UeweixinVoteItem> util = new ExcelUtil<UeweixinVoteItem>(UeweixinVoteItem.class);
        util.exportExcel(response, list, "voteItem");
    }

    /**
     * 获取投票活动选项详细信息
     */
    @PreAuthorize(hasPermi = "voteItem:voteItem:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ueweixinVoteItemService.selectUeweixinVoteItemById(id));
    }

    /**
     * 新增投票活动选项
     */
    @PreAuthorize(hasPermi = "voteItem:voteItem:add")
    @Log(title = "投票活动选项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UeweixinVoteItem ueweixinVoteItem)
    {
        if(StringUtils.isNull(ueweixinVoteItem.getVoteItemCount())){
                ueweixinVoteItem.setVoteItemCount(0L);
        }
        return toAjax(ueweixinVoteItemService.insertUeweixinVoteItem(ueweixinVoteItem));
    }

    /**
     * 修改投票活动选项
     */
    @PreAuthorize(hasPermi = "voteItem:voteItem:edit")
    @Log(title = "投票活动选项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UeweixinVoteItem ueweixinVoteItem)
    {
        return toAjax(ueweixinVoteItemService.updateUeweixinVoteItem(ueweixinVoteItem));
    }

    /**
     * 删除投票活动选项
     */
    @PreAuthorize(hasPermi = "voteItem:voteItem:remove")
    @Log(title = "投票活动选项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ueweixinVoteItemService.deleteUeweixinVoteItemByIds(ids));
    }



}

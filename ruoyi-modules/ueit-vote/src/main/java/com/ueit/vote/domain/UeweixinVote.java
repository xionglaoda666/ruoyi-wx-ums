package com.ueit.vote.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 投票活动对象 ueweixin_vote
 * 
 * @author cuixh
 * @date 2021-03-08
 */
public class UeweixinVote extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 关键词 */
    private String keyword;

    /** token */
    private String token;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 图片 */
    @Excel(name = "图片")
    private String picurl;

    /** 是否显示图片 */
    private String showpic;

    /** 活动简介 */
    @Excel(name = "活动简介")
    private String info;

    /** 奖项设置 */
    private String prizeInfo;

    /** 投票规则 */
    private String voteRoles;

    /** 活动开始时间 */
    @Excel(name = "活动开始时间")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date statdate;

    /** 活动结束时间 */
    @Excel(name = "活动结束时间")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date enddate;

    /** 限制每个用户每个活动总的投票数，超过不让投票,0表示不限制,默认为0,请输入整数 */
    @Excel(name = "限制每个用户每个活动总的投票数，超过不让投票,0表示不限制,默认为0,请输入整数")
    private Long allLimitNum;

    /** 限制每个用户每个活动每天的投票数，超过不让投票,0表示不限制,默认为0,请输入整数 */
    @Excel(name = "限制每个用户每个活动每天的投票数，超过不让投票,0表示不限制,默认为0,请输入整数")
    private Long dayLimitNum;

    /** 限制每个选项每天获得的票数,0表示不限制,1表示每个用户每天只能给该选项投1票,默认为0,请输入整数 */
    @Excel(name = "限制每个选项每天获得的票数,0表示不限制,1表示每个用户每天只能给该选项投1票,默认为0,请输入整数")
    private Long selecterLimitNum;

    /** 活动状态 */
    @Excel(name = "活动状态")
    private String status;

    private String subscribe;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setKeyword(String keyword) 
    {
        this.keyword = keyword;
    }

    public String getKeyword() 
    {
        return keyword;
    }
    public void setToken(String token) 
    {
        this.token = token;
    }

    public String getToken() 
    {
        return token;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setPicurl(String picurl) 
    {
        this.picurl = picurl;
    }

    public String getPicurl() 
    {
        return picurl;
    }
    public void setShowpic(String  showpic)
    {
        this.showpic = showpic;
    }

    public String  getShowpic()
    {
        return showpic;
    }
    public void setInfo(String info) 
    {
        this.info = info;
    }

    public String getInfo() 
    {
        return info;
    }
    public void setPrizeInfo(String prizeInfo) 
    {
        this.prizeInfo = prizeInfo;
    }

    public String getPrizeInfo() 
    {
        return prizeInfo;
    }
    public void setVoteRoles(String voteRoles) 
    {
        this.voteRoles = voteRoles;
    }

    public String getVoteRoles() 
    {
        return voteRoles;
    }

    public Date getStatdate() {
        return statdate;
    }

    public void setStatdate(Date statdate) {
        this.statdate = statdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public void setAllLimitNum(Long allLimitNum)
    {
        this.allLimitNum = allLimitNum;
    }

    public Long getAllLimitNum() 
    {
        return allLimitNum;
    }
    public void setDayLimitNum(Long dayLimitNum) 
    {
        this.dayLimitNum = dayLimitNum;
    }

    public Long getDayLimitNum() 
    {
        return dayLimitNum;
    }
    public void setSelecterLimitNum(Long selecterLimitNum) 
    {
        this.selecterLimitNum = selecterLimitNum;
    }

    public Long getSelecterLimitNum() 
    {
        return selecterLimitNum;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(String subscribe) {
        this.subscribe = subscribe;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("keyword", getKeyword())
            .append("token", getToken())
            .append("type", getType())
            .append("picurl", getPicurl())
            .append("showpic", getShowpic())
            .append("info", getInfo())
            .append("prizeInfo", getPrizeInfo())
            .append("voteRoles", getVoteRoles())
            .append("statdate", getStatdate())
            .append("enddate", getEnddate())
            .append("allLimitNum", getAllLimitNum())
            .append("dayLimitNum", getDayLimitNum())
            .append("selecterLimitNum", getSelecterLimitNum())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
                .append("subscribe",getSubscribe())
            .toString();
    }
}

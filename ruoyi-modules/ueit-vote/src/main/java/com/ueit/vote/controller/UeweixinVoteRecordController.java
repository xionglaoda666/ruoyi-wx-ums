package com.ueit.vote.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ueit.vote.domain.UeweixinVoteRecord;
import com.ueit.vote.service.IUeweixinVoteRecordService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 投票记录Controller
 * 
 * @author cuixh
 * @date 2021-03-08
 */
@RestController
@RequestMapping("/record")
public class UeweixinVoteRecordController extends BaseController
{
    @Autowired
    private IUeweixinVoteRecordService ueweixinVoteRecordService;

    /**
     * 查询投票记录列表
     */
    @PreAuthorize(hasPermi = "record:record:list")
    @GetMapping("/list")
    public TableDataInfo list(UeweixinVoteRecord ueweixinVoteRecord)
    {
        startPage();
        List<UeweixinVoteRecord> list = ueweixinVoteRecordService.selectUeweixinVoteRecordList(ueweixinVoteRecord);
        return getDataTable(list);
    }

    /**
     * 导出投票记录列表
     */
    @PreAuthorize(hasPermi = "record:record:export")
    @Log(title = "投票记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UeweixinVoteRecord ueweixinVoteRecord) throws IOException
    {
        List<UeweixinVoteRecord> list = ueweixinVoteRecordService.selectUeweixinVoteRecordList(ueweixinVoteRecord);
        ExcelUtil<UeweixinVoteRecord> util = new ExcelUtil<UeweixinVoteRecord>(UeweixinVoteRecord.class);
        util.exportExcel(response, list, "record");
    }

    /**
     * 获取投票记录详细信息
     */
    @PreAuthorize(hasPermi = "record:record:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ueweixinVoteRecordService.selectUeweixinVoteRecordById(id));
    }

    /**
     * 新增投票记录
     */
    @PreAuthorize(hasPermi = "record:record:add")
    @Log(title = "投票记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UeweixinVoteRecord ueweixinVoteRecord)
    {
        return toAjax(ueweixinVoteRecordService.insertUeweixinVoteRecord(ueweixinVoteRecord));
    }

    /**
     * 修改投票记录
     */
    @PreAuthorize(hasPermi = "record:record:edit")
    @Log(title = "投票记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UeweixinVoteRecord ueweixinVoteRecord)
    {
        return toAjax(ueweixinVoteRecordService.updateUeweixinVoteRecord(ueweixinVoteRecord));
    }

    /**
     * 删除投票记录
     */
    @PreAuthorize(hasPermi = "record:record:remove")
    @Log(title = "投票记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ueweixinVoteRecordService.deleteUeweixinVoteRecordByIds(ids));
    }
}

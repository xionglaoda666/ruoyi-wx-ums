package com.ueit.vote.vo;

import lombok.Data;

@Data
public class UeweixinVoteVO {
    private String id;
    private String title;
}

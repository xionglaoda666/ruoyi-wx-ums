package com.ueit.vote.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.date.DateTime;
import com.ueit.vote.vo.UeweixinVoteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ueit.vote.domain.UeweixinVote;
import com.ueit.vote.service.IUeweixinVoteService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 投票活动Controller
 * 
 * @author cuixh
 * @date 2021-03-08
 */
@RestController
@RequestMapping("/vote")
public class UeweixinVoteController extends BaseController
{
    @Autowired
    private IUeweixinVoteService ueweixinVoteService;

    /**
     * 查询投票活动列表
     */
    @PreAuthorize(hasPermi = "vote:vote:list")
    @GetMapping("/list")
    public TableDataInfo list(UeweixinVote ueweixinVote)
    {
        startPage();
        List<UeweixinVote> list = ueweixinVoteService.selectUeweixinVoteList(ueweixinVote);
        return getDataTable(list);
    }

    /**
     * 查询投票活动列表,作为下拉选项使用
     */
    @PreAuthorize(hasPermi = "vote:vote:list")
    @GetMapping("/listToType")
    public AjaxResult listToType(UeweixinVote ueweixinVote)
    {
        List<UeweixinVote> list = ueweixinVoteService.selectUeweixinVoteList(ueweixinVote);
        List<UeweixinVoteVO> result = new ArrayList<>();
        for (UeweixinVote item:list
             ) {
            UeweixinVoteVO entity =  new UeweixinVoteVO();
            entity.setId(item.getToken());
            entity.setTitle(item.getTitle());
            result.add(entity);
        }
        return AjaxResult.success(result);
    }

    /**
     * 导出投票活动列表
     */
    @PreAuthorize(hasPermi = "vote:vote:export")
    @Log(title = "投票活动", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UeweixinVote ueweixinVote) throws IOException
    {
        List<UeweixinVote> list = ueweixinVoteService.selectUeweixinVoteList(ueweixinVote);
        ExcelUtil<UeweixinVote> util = new ExcelUtil<UeweixinVote>(UeweixinVote.class);
        util.exportExcel(response, list, "vote");
    }

    /**
     * 获取投票活动详细信息
     */
    @PreAuthorize(hasPermi = "vote:vote:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ueweixinVoteService.selectUeweixinVoteById(id));
    }

    /**
     * 新增投票活动
     */
    @PreAuthorize(hasPermi = "vote:vote:add")
    @Log(title = "投票活动", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UeweixinVote ueweixinVote)
    {
        return toAjax(ueweixinVoteService.insertUeweixinVote(ueweixinVote));
    }

    /**
     * 修改投票活动
     */
    @PreAuthorize(hasPermi = "vote:vote:edit")
    @Log(title = "投票活动", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UeweixinVote ueweixinVote)
    {
        return toAjax(ueweixinVoteService.updateUeweixinVote(ueweixinVote));
    }

    /**
     * 删除投票活动
     */
    @PreAuthorize(hasPermi = "vote:vote:remove")
    @Log(title = "投票活动", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ueweixinVoteService.deleteUeweixinVoteByIds(ids));
    }

}

package com.ruoyi.captcha.controller;

import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/captcha")
public class VerificationController {
    @Autowired
    private CaptchaService captchaService;


    @GetMapping("/verification")
    public AjaxResult verification(@RequestParam("captchaVerification") String verification){
        CaptchaVO captchaVO = new CaptchaVO();
        captchaVO.setCaptchaVerification(verification);
        ResponseModel response = captchaService.verification(captchaVO);
        return  AjaxResult.success(response);
    }
}

package com.ruoyi.ums.ums.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.api.domain.UmsAccountInfo;
import com.ruoyi.ums.ums.mapper.UmsAccountInfoMapper;
import com.ruoyi.ums.ums.service.IUmsAccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员用户信息Service业务层处理
 * 
 * @author ueit
 * @date 2021-03-03
 */
@Service
public class UmsAccountInfoServiceImpl implements IUmsAccountInfoService
{
    @Autowired
    private UmsAccountInfoMapper umsAccountInfoMapper;

    /**
     * 查询会员用户信息
     * 
     * @param accountId 会员用户信息ID
     * @return 会员用户信息
     */
    @Override
    public UmsAccountInfo selectUmsAccountInfoById(String accountId)
    {
        return umsAccountInfoMapper.selectUmsAccountInfoById(accountId);
    }

    /**
     * 查询会员用户信息列表
     * 
     * @param umsAccountInfo 会员用户信息
     * @return 会员用户信息
     */
    @Override
    public List<UmsAccountInfo> selectUmsAccountInfoList(UmsAccountInfo umsAccountInfo)
    {
        return umsAccountInfoMapper.selectUmsAccountInfoList(umsAccountInfo);
    }

    /**
     * 新增会员用户信息
     * 
     * @param umsAccountInfo 会员用户信息
     * @return 结果
     */
    @Override
    public int insertUmsAccountInfo(UmsAccountInfo umsAccountInfo)
    {
        umsAccountInfo.setCreateTime(DateUtils.getNowDate());
        return umsAccountInfoMapper.insertUmsAccountInfo(umsAccountInfo);
    }

    /**
     * 修改会员用户信息
     * 
     * @param umsAccountInfo 会员用户信息
     * @return 结果
     */
    @Override
    public int updateUmsAccountInfo(UmsAccountInfo umsAccountInfo)
    {
        return umsAccountInfoMapper.updateUmsAccountInfo(umsAccountInfo);
    }

    /**
     * 批量删除会员用户信息
     * 
     * @param accountIds 需要删除的会员用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUmsAccountInfoByIds(String[] accountIds)
    {
        return umsAccountInfoMapper.deleteUmsAccountInfoByIds(accountIds);
    }

    /**
     * 删除会员用户信息信息
     * 
     * @param accountId 会员用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUmsAccountInfoById(String accountId)
    {
        return umsAccountInfoMapper.deleteUmsAccountInfoById(accountId);
    }
}

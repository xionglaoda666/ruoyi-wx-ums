package com.ruoyi.ums.ums.controller;

import java.util.List;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.api.domain.UmsAccountInfo;
import com.ruoyi.ums.ums.service.IUmsAccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 会员用户信息Controller
 * 
 * @author ueit
 * @date 2021-03-03
 */
@RestController
@RequestMapping("/umsInfo")
public class UmsAccountInfoController extends BaseController
{
    @Autowired
    private IUmsAccountInfoService umsAccountInfoService;


    /**
     * 查询会员用户信息列表
     */
    @PreAuthorize(hasPermi = "ums:umsInfo:list")
    @GetMapping("/list")
    public TableDataInfo list(UmsAccountInfo umsAccountInfo)
    {
        startPage();
        List<UmsAccountInfo> list = umsAccountInfoService.selectUmsAccountInfoList(umsAccountInfo);
        return getDataTable(list);
    }

    /**
     * 导出会员用户信息列表
     */
    @PreAuthorize(hasPermi = "ums:umsInfo:export")
    @Log(title = "会员用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UmsAccountInfo umsAccountInfo) throws IOException
    {
        List<UmsAccountInfo> list = umsAccountInfoService.selectUmsAccountInfoList(umsAccountInfo);
        ExcelUtil<UmsAccountInfo> util = new ExcelUtil<UmsAccountInfo>(UmsAccountInfo.class);
        util.exportExcel(response, list, "umsInfo");
    }

    /**
     * 获取会员用户信息详细信息
     */
    @PreAuthorize(hasPermi = "ums:umsInfo:query")
    @GetMapping(value = "/{accountId}")
    public AjaxResult getInfo(@PathVariable("accountId") String accountId)
    {
        return AjaxResult.success(umsAccountInfoService.selectUmsAccountInfoById(accountId));
    }

    /**
     * 新增会员用户信息
     */
    @PreAuthorize(hasPermi = "ums:umsInfo:add")
    @Log(title = "会员用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UmsAccountInfo umsAccountInfo)
    {
        return toAjax(umsAccountInfoService.insertUmsAccountInfo(umsAccountInfo));
    }

    /**
     * 修改会员用户信息
     */
    @PreAuthorize(hasPermi = "ums:umsInfo:edit")
    @Log(title = "会员用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UmsAccountInfo umsAccountInfo)
    {
        return toAjax(umsAccountInfoService.updateUmsAccountInfo(umsAccountInfo));
    }

    /**
     * 删除会员用户信息
     */
    @PreAuthorize(hasPermi = "ums:umsInfo:remove")
    @Log(title = "会员用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{accountIds}")
    public AjaxResult remove(@PathVariable String[] accountIds)
    {
        return toAjax(umsAccountInfoService.deleteUmsAccountInfoByIds(accountIds));
    }

}

package com.ruoyi.ums.auth.service;

import java.util.List;

import com.ruoyi.system.api.domain.UmsAccountAuth;

/**
 * 授权信息Service接口
 * 
 * @author ruoyi
 * @date 2021-03-03
 */
public interface IUmsAccountAuthService 
{
    /**
     * 查询授权信息
     * 
     * @param id 授权信息ID
     * @return 授权信息
     */
    public UmsAccountAuth selectUmsAccountAuthById(Long id);

    /**
     * 查询授权信息列表
     * 
     * @param umsAccountAuth 授权信息
     * @return 授权信息集合
     */
    public List<UmsAccountAuth> selectUmsAccountAuthList(UmsAccountAuth umsAccountAuth);

    /**
     * 新增授权信息
     * 
     * @param umsAccountAuth 授权信息
     * @return 结果
     */
    public int insertUmsAccountAuth(UmsAccountAuth umsAccountAuth);

    public UmsAccountAuth selectUmsAccountInfoByIdentifier(UmsAccountAuth umsAccountAuth);

    /**
     * 修改授权信息
     * 
     * @param umsAccountAuth 授权信息
     * @return 结果
     */
    public int updateUmsAccountAuth(UmsAccountAuth umsAccountAuth);

    /**
     * 批量删除授权信息
     * 
     * @param ids 需要删除的授权信息ID
     * @return 结果
     */
    public int deleteUmsAccountAuthByIds(Long[] ids);

    /**
     * 删除授权信息信息
     * 
     * @param id 授权信息ID
     * @return 结果
     */
    public int deleteUmsAccountAuthById(Long id);
}
